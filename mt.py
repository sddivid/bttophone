#!/usr/bin/python
# -*- coding: UTF-8 -*-


import multiprocessing as mp

import time, os


import serial
import io


namelist = ["",'serACM0', 'serACM1', 'serACM2', 'serACM3','serUSB0','serUSB1','serUSB2','serUSB3'];


Usblist = ["",'serACM0', 'serACM1', 'serACM2', 'serACM3','serUSB0','serUSB1','serUSB2','serUSB3'];

ttylist = ["",'/dev/ttyACM0', '/dev/ttyACM1', '/dev/ttyACM2', '/dev/ttyACM3','/dev/ttyUSB0','/dev/ttyUSB1','/dev/ttyUSB2','/dev/ttyUSB3'];


count = 0


#def my_process(num):
#  print(num)
#   print("I'm process no.%d, PID = %d." %(num, os.getpid()))

#    time.sleep(3)

def my_process(num):
    print("I'm process no.%d, PID = %d." %(num, os.getpid()))

    try:
        #serACM0 = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
        #VAIABLE_A = Usblist[num]
        Usblist[num] = serial.Serial(ttylist[num], 9600, timeout=1)
        print("name: ", Usblist[num].name)
        #打印設備名稱
        print("port: ", Usblist[num].port)
        #打印設備名
        Usblist[num].open
        ()  #打開端口
        

        prev_data = ""
        while(100):
            data = Usblist[num].readline()
            #data = ser.readlines()
            #ser.xreadlines()
            data.rstrip()
    #        print(data)
            data_str = str(data)
            
            if data_str != "b''":
                if data_str != prev_data:
                    print("new data: ", data_str)
                
                prev_data = data_str

                f = open("./data/"+namelist[num]+".json", 'w+')
                f.write(namelist[num]+"\n")
                f.write(data_str.strip('\n'))
                # 关闭打开的文件
                f.close()
            else:
                print("no data")

        Usblist[num].close()
        
    except: 
        print("Error: 没有找到設備",num,"或读取設備失败")
    else:
        print("读取設備内容成功",ttylist[num])



print("Creating new processes...")   

p1 = mp.Process(target=my_process, args=(1,))

p2 = mp.Process(target=my_process, args=(2,))

p3 = mp.Process(target=my_process, args=(3,))

p4 = mp.Process(target=my_process, args=(4,))

p5 = mp.Process(target=my_process, args=(5,))

p6 = mp.Process(target=my_process, args=(6,))

p7 = mp.Process(target=my_process, args=(7,))

p8 = mp.Process(target=my_process, args=(8,))

p1.start()
time.sleep( 0.005 )
p2.start()
time.sleep( 0.005 )
p3.start()
time.sleep( 0.005 )
p4.start()
time.sleep( 0.005 )
p5.start()
time.sleep( 0.005 )
p6.start()
time.sleep( 0.005 )
p7.start()
time.sleep( 0.005 )
p8.start()

print("Waiting for all the processes...")

p1.join()
p2.join()
p3.join()
p4.join()
p5.join()
p6.join()
p7.join()
p8.join()



